package com.sourceit.lesson4;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements InteractListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_left, LeftFragment.getInstance())
                .commit();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container_right, RightFragment.getInstance())
                .commit();
    }

    public void show(String string) {
        LeftFragment.getInstance().show(string);
    }
}

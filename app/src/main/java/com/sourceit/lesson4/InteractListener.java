package com.sourceit.lesson4;

/**
 * Created by wenceslaus on 26.05.18.
 */

public interface InteractListener {
    void show(String text);
}

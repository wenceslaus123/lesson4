package com.sourceit.lesson4;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by wenceslaus on 26.05.18.
 */

public class RightFragment extends Fragment {

    private static RightFragment instance;

    public static RightFragment getInstance() {
        if (instance == null) {
            instance = new RightFragment();
        }
        return instance;
    }

    EditText editText;
    InteractListener interactListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof InteractListener) {
            interactListener = (InteractListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_right, container, false);
        editText = (EditText) root.findViewById(R.id.input);
        root.findViewById(R.id.action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (interactListener != null) {
                    interactListener.show(editText.getText().toString());
                }
            }
        });
        return root;
    }

}

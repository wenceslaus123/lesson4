package com.sourceit.lesson4;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by wenceslaus on 26.05.18.
 */

public class LeftFragment extends Fragment implements InteractListener {

    private static LeftFragment instance;
    TextView label;

    public static LeftFragment getInstance() {
        if (instance == null) {
            instance = new LeftFragment();
        }
        return instance;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_left, container, false);
        label = (TextView) root.findViewById(R.id.label);
        return root;
    }

    @Override
    public void show(String text) {
        label.setText(text);
    }
}
